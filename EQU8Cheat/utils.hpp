#pragma once
#include <stdarg.h>
#include <ntstrsafe.h>
#include <ntifs.h>

NTSTATUS OpenFile(__in LPCWSTR FilePath, __in BOOLEAN Write, __in BOOLEAN Append, __out PHANDLE Handle)
{
    UNICODE_STRING      wzFilePath;
    OBJECT_ATTRIBUTES   objAttributes;
    IO_STATUS_BLOCK     ioStatus;
    RtlInitUnicodeString(&wzFilePath, FilePath);
    InitializeObjectAttributes(&objAttributes, &wzFilePath, OBJ_KERNEL_HANDLE, NULL, NULL);

    ACCESS_MASK mask = SYNCHRONIZE;

    mask |= Write ? (Append ? FILE_APPEND_DATA : FILE_WRITE_DATA) : FILE_READ_DATA;

    return ZwCreateFile(Handle,
        mask,
        &objAttributes, &ioStatus,
        NULL, FILE_ATTRIBUTE_NORMAL,
        FILE_SHARE_READ, FILE_OPEN_IF,
        FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);
}

NTSTATUS LogToFile(__in LPCSTR Format, __in_opt ...)
{
    IO_STATUS_BLOCK     ioStatus;
    HANDLE              hFile;
    CHAR                buffer[512];
    size_t              cb;

    NTSTATUS status = OpenFile(L"\\SystemRoot\\Temp\\log.log", TRUE, TRUE, &hFile);

    if (NT_SUCCESS(status))
    {
        RtlZeroMemory(buffer, 512);
        va_list va;
        va_start(va, Format);
        RtlStringCbVPrintfA(buffer, 512, Format, va);
        va_end(va);

        RtlStringCbLengthA(buffer, sizeof(buffer), &cb);
        ZwWriteFile(hFile, NULL, NULL, NULL, &ioStatus, buffer, (ULONG)cb, NULL, NULL);

        ZwFlushBuffersFile(hFile, &ioStatus);
        ZwClose(hFile);
        return STATUS_SUCCESS;
    }
    return status;
}

ULONG64 ResolveInstructionOffset(PCHAR instruction, ULONG64 offsetToEIP, ULONG64 offsetOfDataPtr)
{
    ULONG64 EIP = (ULONG64)instruction + offsetToEIP;	//instruction pointer to add offset to
    LONG32 offset = *(LONG32*)(instruction + offsetOfDataPtr);	//get the data ptr offset from the current EIP
    return EIP + offset;
}


NTSTATUS ZwQuerySystemInformation(
    _In_      SYSTEM_INFORMATION_CLASS SystemInformationClass,
    _Inout_   PVOID                    SystemInformation,
    _In_      ULONG                    SystemInformationLength,
    _Out_opt_ PULONG                   ReturnLength
);
uintptr_t GetSystemModuleBase(const char* module_name)
{
    ULONG bytes = 0;
    NTSTATUS status = ZwQuerySystemInformation(SystemModuleInformation, 0, bytes, &bytes);

    if (!bytes)
        return 0;

    PRTL_PROCESS_MODULES modules = (PRTL_PROCESS_MODULES)ExAllocatePool(NonPagedPool, bytes);

    status = ZwQuerySystemInformation(SystemModuleInformation, modules, bytes, &bytes);

    if (!NT_SUCCESS(status))
        return 0;

    PRTL_PROCESS_MODULE_INFORMATION module = modules->Modules;
    uintptr_t module_base = 0, module_size = 0;

    for (ULONG i = 0; i < modules->NumberOfModules; i++)
    {
        USHORT offset = module[i].OffsetToFileName;
        char* name = reinterpret_cast<char*>(reinterpret_cast<uintptr_t>(module[i].FullPathName) + offset);
        if (strcmp(name, module_name) == 0)
        {
            module_base = (uintptr_t)module[i].ImageBase;
            break;
        }
    }

    if (modules)
        ExFreePool(modules);

    if (module_base <= 0)
        return 0;

    return module_base;
}